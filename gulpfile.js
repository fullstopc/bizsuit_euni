
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browsersync = require('browser-sync'),
    php = require('gulp-connect-php');

function style(){
    return (
        gulp.src('scss/app.scss')
        .pipe(sass()).on('error', sass.logError)
        .pipe(gulp.dest('public/css'))
        .pipe(browsersync.stream())
    );
}

function reload(){
    browsersync.reload();
}

function sync(){
    php.server({}, function (){
        browsersync({
            proxy: 'localhost/bizsuit_euni/public'
        });

        gulp.watch(['./scss/app.scss','./scss/layout.scss', './public/index.php' ]).on('change', function () {
            style();
            browsersync.reload();
        });
    });
}

exports.style = style;
exports.reload = reload;
exports.sync = sync;


// var gulp = require('gulp'),
//     sass = require('gulp-sass'),
//     browsersync = require('browser-sync').create();

// function style(){
//     return (
//         gulp.src('scss/app.scss')
//         .pipe(sass()).on('error', sass.logError)
//         .pipe(gulp.dest('public/css'))
//     );
// }

// function reload(){
//     browsersync.reload();
// }

// function watch(){
//     browsersync.init({
//         server:{
//             baseDir:"public"
//         }
//     });
//     gulp.watch('scss/app.scss', gulp.series(
//         style,
//         reload
//     ));
// }

// exports.watch = watch;
// exports.style = style;
// exports.reload = reload;

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name='keywords' content='' />

    <title>EUNISOFT TECHNOLOGY</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor.js"></script>    
    <script src="vendor/slick-carousel/slick.min.js"></script>
    <link rel="stylesheet" href="vendor/slick-carousel/slick.css">

</head>
<body>
  <div id="wrapper">
    <header id="header">
        <div class="header-logo">
            <a><img class="img-fluid mx-5" src="img/Logo.png"/></a>
        </div>
        <div class="header-nav navbar navbar-dark navbar-expand-lg">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNavbar" aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <div class="header-hamburger">
                <div class="hamburger-nav">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            </button>
            <div class="collapse navbar-collapse" id="headerNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item active"><a class="nav-link" href="#block_banner">HOME</a></li>
                    <li class="nav-item"><a class="nav-link" href="#block_product">EUNISOFT SYSTEM</a></li>
                    <li class="nav-item"><a class="nav-link" href="#block_product">CYBERCAFE SOLUTION</a></li>
                    <li class="nav-item"><a class="nav-link" href="#block_aboutus">ABOUT US</a></li>                        
                    <li class="nav-item"><a class="nav-link" href="#block_contactus">CONTACT US</a></li>
                </ul>
            </div>
        </div>
    </header>
    <section id="content">
        <div class="block block-banner banner banner-main" id="block_banner">
            <div class="banner-list">
                <div class="banner-item">
                    <div class="banner-img" style="background-image:url('img/banner-1.jpg');"></div>
                    <div class="banner-content d-flex">
                        <div class="row align-items-center w-100 m-0">
                            <div class="col p-0">
                                <div class="text-center">
                                    <h1 >CYBERCAFE SOLUTION</h1>
                                    <i>
                                    Provide Internet cafe diskless management system<br/>
                                    Game/movie automatic update
                                    </i>
                                </div>
                                <br/>
                                <div class="row p-sm-5 m-sm-5 m-0 p-0 pt-5">
                                    <div class="col-lg-6 col-12 mx-auto d-flex justify-content-between">
                                        <a href="" class="btn btn-outline-secondary">                            
                                            <span class="mx-4">Read More</span>
                                        </a>                        
                                        <a href="" class="btn btn-outline-secondary">                            
                                            <span class="mx-4">Enquiry Us</span>
                                        </a>
                                    </div>
                                </div>    
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="block block-products" id="block_products">
            <div class="row py-5">
                <div class="col-10 mx-auto py-5">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <h1 class="mb-5">What is Eunisoft?</h1>
                            <p class="d-flex justify-content-center align-items-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="block block-product2" id="block_product2">
            <div class="row">
                <div class="col-lg-8 col-md-9 col-sm-10 col-11 mx-auto text-center">
                    <div class="my-4 pb-2">
                        <h2>Eunisoft System</h2>
                        <p>All-in-one Cyber Cafe Centralized System
                            <br>
                            Everything is under your control
                        </p>
                    </div>
                    <div class="dots-container mb-lg-0 mb-5">
                        
                    </div>
                    <div class="product-slider">
                        <div class="slider-item" data-desc="POS Management" data-icon="fas fa-file-invoice-dollar">
                            <div class="row">
                                <div class="col-sm-6 col-12 text-left slider-text mt-sm-0 mt-5 order-sm-1 order-2">
                                    <h2 class="mb-3">POS Management</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <button type="button" class="btn btn-outline-secondary px-5 my-4">Read More</button>
                                </div>
                                <div class="col-sm-6 col-12 order-sm-2 order-1">
                                    <img class="img-fluid" src="img/aboutus-content-1.jpg" />
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" data-desc="Membership System" data-icon="fas fa-users">
                            <div class="row">
                                <div class="col-sm-6 col-12 text-left slider-text mt-sm-0 mt-5 order-sm-1 order-2">
                                    <h2 class="mb-3">Membership System</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <button type="button" class="btn btn-outline-secondary px-5 my-4">Read More</button>
                                </div>
                                <div class="col-sm-6 col-12 order-sm-2 order-1">
                                    <img class="img-fluid" src="img/aboutus-content-2.jpg" />
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" data-desc="Scheduled Updates" data-icon="fas fa-calendar-check">
                            <div class="row">
                                <div class="col-sm-6 col-12 text-left slider-text mt-sm-0 mt-5 order-sm-1 order-2">
                                    <h2 class="mb-3">Scheduled Updates</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <button type="button" class="btn btn-outline-secondary px-5 my-4">Read More</button>
                                </div>
                                <div class="col-sm-6 col-12 order-sm-2 order-1">
                                    <img class="img-fluid" src="img/aboutus-content-3.jpg" />
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" data-desc="24-Hours Support" data-icon="fas fa-user-clock">
                            <div class="row">
                                <div class="col-sm-6 col-12 text-left slider-text mt-sm-0 mt-5 order-sm-1 order-2">
                                    <h2 class="mb-3">24-Hours Support</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <button type="button" class="btn btn-outline-secondary px-5 my-4">Read More</button>
                                </div>
                                <div class="col-sm-6 col-12 order-sm-2 order-1">
                                    <img class="img-fluid" src="img/aboutus-content-4.jpg" />
                                </div>
                            </div>
                        </div>
                        <div class="slider-item" data-desc="Prepaid System" data-icon="fas fa-coins">
                            <div class="row">
                                <div class="col-sm-6 col-12 text-left slider-text mt-sm-0 mt-5 order-sm-1 order-2">
                                    <h2 class="mb-3">Prepaid System</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <button type="button" class="btn btn-outline-secondary px-5 my-4">Read More</button>
                                </div>
                                <div class="col-sm-6 col-12 order-sm-2 order-1">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block-aboutus" id="block_aboutus">            
            <div class="row">
                <div class="col-xl-9 col-11 mx-auto p-5">
                    <div class="row align-items-center">
                        <div class="col-xl-9 col-sm-8">
                            <h2 class="d-inline">We Provide one-stop Business Solution</h2>                    
                            <p class="pt-3">Starting a business isn't easy. <br>
                            Why not inquiry with us and find out how to kick-start your business?</p>                    
                        </div>
                        <div class="col-xl-3 col-sm-4">
                            <a class="btn btn-outline-secondary px-5 mt-sm-0 mt-5">LEARN MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row block-aboutus-content">
                <div class="col-11 mx-auto py-5">
                    <div class="row">
                        <div class="col-lg-6 col-12 px-xl-5 px-lg-0 px-3">
                            <div class="p-lg-5 py-3 m-auto">
                                <h1 class="mb-5">Business Blueprint</h1>
                                <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </p>                        
                            </div>
                        </div>
                        <div class="col-lg-6 col-12 px-xl-5 px-lg-0 px-3">
                            <div class="p-lg-5 py-3 m-auto">
                                <h1 class="mb-5">Shopfront Theme</h1>
                                <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </p>                        
                            </div>
                        </div>            
                        <div class="col-lg-6 col-12 px-xl-5 px-lg-0 px-3">
                            <div class="p-lg-5 py-3 m-auto">
                                <h1 class="mb-5">Device & Equipment</h1>
                                <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </p>                        
                            </div>
                        </div>            
                        <div class="col-lg-6 col-12 px-xl-5 px-lg-0 px-3">
                            <div class="p-lg-5 py-3 m-auto">                    
                                <h1 class="mb-5">Training & Management</h1>
                                <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                </p>                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block_customer" class="block block-customer text-center p-5">
            <div class="row">
                <div class="col">
                    <h1 class="mx-md-5 my-5">Over 50+ Customers from West Malaysia to East Malaysia</h1>
                    <div class="row mx-lg-5 px-lg-5">
                        <div class="col-4">
                            <img class="img-fluid" src="img/partner-hp.png"/>
                        </div>
                        <div class="col-4">
                            <img class="img-fluid" src="img/partner-hp.png"/>
                        </div>
                        <div class="col-4">
                            <img class="img-fluid" src="img/partner-hp.png"/>
                        </div>
                    </div>
                    <h3 class="float-right p-sm-5 p-0 py-5">View more</h3>
                </div>
            </div>
        </div>
        <div class="block block-contactus text-center" id="block_contactus">
            <div class="row">
                <div class="col-lg-8 col-10 mx-auto">
                    <div class="my-5">
                        <h1>CONTACT US</h1>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                        </p>
                    </div>
                    <div class="my-5">
                        <div class="row">
                            <div class="col-md-7 col-12 contactus-content py-5 order-md-1 order-2">
                                <div>
                                    <i class="fa fa-home my-2 mx-4"></i>
                                    <p class="text-left mt-0">
                                    01-36, Jalan Austin Perdana 3/10,<br>
                                    Taman Austin Perdana,<br>
                                    81100 Johor Bahru, Malaysia
                                    </p>
                                </div>
                                <div>
                                    <i class="fa fa-phone my-2 mx-4"></i>
                                    <p class="text-left mt-0">
                                    011-2876 3250
                                    </p>
                                </div>
                                <div>
                                    <i class="fa fa-at my-2 mx-4"></i>
                                    <p class="text-left mt-0">
                                    eunisoft@gmail.com
                                    </p>
                                </div>
                                <div>
                                    <i class="fab fa-facebook my-2 mx-4"></i>
                                    <p class="text-left mt-0" style="hyphens:auto;">
                                    www.facebook.com/eunisoft&shy;technology
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-5 col-12 contactus-form text-left order-md-2 order-1">
                                <p>FILL OUT THIS FORM</p>
                                <input type="text" class="form-control mb-3" placeholder="YOUR NAME"/>
                                <input type="text" class="form-control mb-3" placeholder="YOUR EMAIL"/>
                                <textarea type="text" class="form-control mb-3" rows="5" placeholder="HELLO!"></textarea>
                                <div class="text-center my-4"><button class="btn btn-outline-secondary px-5">Submit</button></div>
                            </div>                            
                        </div>
                    </div>
                    <footer class="mb-5" id="footer">Copyright © <?php echo date("Y"); ?> Eunisoft Technology Enterprise</footer>
                </div>
            </div>
        </div>
    </section>    
  </div>
</body>
<script>
$(window).on("scroll", function () {
    if ($(this).scrollTop() > 100) {
        $("#header").addClass("scroll");
    }
    else {
        $("#header").removeClass("scroll");
    }
});

$(function(){
    $(".product-slider").slick({

        autoplay: true,
        dots: true,
        arrows: false,
        fade:true,
        appendDots:'.dots-container',
        customPaging : function(slider, i) {
            var icon = $(slider.$slides[i]).data('icon');
            var desc = $(slider.$slides[i]).data('desc');
            return '<a><span class="fa-stack fa-3x"><i class="fa fa-circle fa-stack-2x"></i><i class="icon '+icon+' fa-stack-1x"></i></span></a><br><p class="text-center p-3">'+desc+'</p>';
        },

        responsive: [{ 
            breakpoint: 786,
            settings: {                
                customPaging : function(slider, i) {
                    var icon = $(slider.$slides[i]).data('icon');                    
                    return '<a><span class="fa-stack fa-2x" style="font-size:30px;"><i class="fa fa-circle fa-stack-2x"></i><i class="icon '+icon+' fa-stack-1x"></i></span></a>';
                },
                pauseOnHover:false,
                pauseOnFocus:false,
            } 
        }]
    });
    $(".hamburger-nav").click(function(){
        $(".header-hamburger").toggleClass('hamburger-active');
    });
});
</script>
</html>